import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {RestaurantsProvider} from '../../providers/restaurants/restaurants';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  restaurants: any = [];
  constructor(public navCtrl: NavController, private restaurantsProvider: RestaurantsProvider) {
    this.restaurants = restaurantsProvider.getRestaurants();
  }

}
