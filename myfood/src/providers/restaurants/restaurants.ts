
import { Injectable } from '@angular/core';

/*
  Generated class for the RestaurantsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestaurantsProvider {

  constructor() {

  }
  getRestaurants(){
    var restaurants =[
      {
        name: "Mega Burguer",
        type: "Fast Food",
        image: "MegaBurguer.png",
        products:[
          {
            id: 1,
            name: "Offer of the day 1",
            image: "burguer1.png",
            price: 20.00
          },
          {
            id: 2,
            name: "Offer of the day 2",
            image: "burguer2.png",
            price: 25.00
          }
        ]
      },
      {
        name: "Big Chin",
        type: "Chinese food",
        image: "bigchin.png",
        products: [
          {
            id: 3,
            name: "Combo offer of the day 1",
            image: "Pasta1.png",
            price: 10.00
          },
          {
            id: 4,
            name: "Combo offer of the day 1",
            image: "Pasta2.png",
            price: 15.00
          }
        ]
      }

    ] 
    return restaurants;   
  }

}
